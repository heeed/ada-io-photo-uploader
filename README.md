# Ada IO photo uploader

Simple python script to upload photos from a Pi to the Adafruit IO service.

Images are base64 encoded before upload.

## Getting started

Update script with Ada IO credentials and run as needed....setting it a cron job will be ideal.

Use imgage block on dashboard to display base64 encoded data as image.
