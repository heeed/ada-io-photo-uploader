import base64
from time import sleep
from picamera import PiCamera
import datetime
from Adafruit_IO import Client, Data

aio = Client('<username>','#aio_key')

image_encode = "/home/pi/image.jpg"

currentdate = datetime.date.today()

def get_base64(image_path):
    with open(image_path, "rb") as img_file:
        return base64.b64encode(img_file.read()).decode('utf-8')

print("Creating camera object....")
camera = PiCamera()
print("Object created ...")
sleep(1)

print("capturing...")
camera.capture('/home/pi/images/'+str(currentdate)+'.jpg')
camera.capture('/home/pi/image.jpg',resize=(250,250))

print("capture done")
sleep(1)

print("Encoding...")
imagedata = get_base64(image_encode)

print("Sending...")
imagefeed = aio.feeds('#feedkey')
aio.send_data(imagefeed.key,imagedata)
print("Sent")




